import os
os.system("sudo apt update")
os.system("sudo apt full-upgrade")

os.system("git clone git@gitlab.com:ubicomp-lab/ubicompi.git") 
os.system("cd ubicompi")
os.system("sudo %s/install.sh"% os.getcwd())
os.system("sudo apt-get install apache2 -y --fix-missing")

os.system("sudo apt-get install dnsmasq hostapd -y --fix-missing")

os.system("sudo systemctl stop dnsmasq")
os.system("sudo systemctl stop hostapd")


os.system("sudo echo \"interface wlan0\" >> /etc/dhcpcd.conf")
os.system("sudo echo \"static ip_address=192.168.4.1/24\" >> /etc/dhcpcd.conf")
os.system("sudo echo \"nohook wpa_supplicant\" >> /etc/dhcpcd.conf") 

os.system("sudo service dhcpcd restart")

os.system("sudo mv /etc/dnsmasq.conf /etc/dnsmasq.conf.orig")  
os.system("sudo touch /etc/dnsmasq.conf")
os.system("sudo sh -c \"echo \"interface=wlan0\" >> /etc/dnsmasq.conf\"")
os.system("sudo sh -c \"echo \"dhcp-range=192.168.4.2,192.168.4.20,255.255.255.0,24h\" >> /etc/dnsmasq.conf\"")  

os.system("sudo systemctl start dnsmasq")

os.system("sudo touch /etc/hostapd/hostapd.conf")
os.system("sudo sh -c \"echo \"interface=wlan0\" >> /etc/hostapd/hostapd.conf\"") 
os.system("sudo sh -c \"echo \"driver=nl80211\" >> /etc/hostapd/hostapd.conf\"") 
os.system("sudo sh -c \"echo \"ssid=UPicomp\" >> /etc/hostapd/hostapd.conf\"") 
os.system("sudo sh -c \"echo \"hw_mode=g\" >> /etc/hostapd/hostapd.conf\"") 
os.system("sudo sh -c \"echo \"channel=7\" >> /etc/hostapd/hostapd.conf\"") 
os.system("sudo sh -c \"echo \"wmm_enabled=0\" >> /etc/hostapd/hostapd.conf\"") 
os.system("sudo sh -c \"echo \"macaddr_acl=0\" >> /etc/hostapd/hostapd.conf\"") 
os.system("sudo sh -c \"echo \"auth_algs=1\" >> /etc/hostapd/hostapd.conf\"") 
os.system("sudo sh -c \"echo \"ignore_broadcast_ssid=0\" >> /etc/hostapd/hostapd.conf\"")
os.system("sudo sh -c \"echo \"wpa=2\" >> /etc/hostapd/hostapd.conf\"") 
os.system("sudo sh -c \"echo \"wpa_passphrase=UPicomp\" >> /etc/hostapd/hostapd.conf\"") 
os.system("sudo sh -c \"echo \"wpa_key_mgmt=WPA-PSK\" >> /etc/hostapd/hostapd.conf\"") 
os.system("sudo sh -c \"echo \"wmm_enabled=0\" >> /etc/hostapd/hostapd.conf\"") 
os.system("sudo sh -c \"echo \"macaddr_acl=0\" >> /etc/hostapd/hostapd.conf\"") 
os.system("sudo sh -c \"echo \"wpa_pairwise=TKIP\" >> /etc/hostapd/hostapd.conf\"") 
os.system("sudo sh -c \"echo \"rsn_pairwise=CCMP\" >> /etc/hostapd/hostapd.conf\"") 

os.system("sudo sh -c \"echo \"DAEMON_CONF=\"/etc/hostapd/hostapd.conf\"\" >> /etc/default/hostapd\"") 

os.system("sudo systemctl unmask hostapd")
os.system("sudo systemctl enable hostapd")
os.system("sudo systemctl start hostapd")

os.system("sudo ./install.sh")
os.system("sudo reboot")
