  //
  // Interface
  //
  var currentShoppingTaskIndex = 0;
  var currentGymTaskIndex = 0;
  var currentLabel = '';
  var currentAnnotation = '';

  var currentShoppingCart = [];

  function toggle_fullscreen(e) {

      var background = document.getElementById("background");

      if (!background) {
          background = document.createElement("div");
          background.id = "background";
          document.body.appendChild(background);
      }

      if (e.className == "fullscreen") {
          e.className = "";
          background.style.display = "none";
      } else {
          e.className = "fullscreen";
          background.style.display = "block";
      }

  }

  function set_display(value) {
      var d = new Date();
      var val;
      d.setTime(d.getTime() + (365 * 24 * 60 * 60 * 1000));
      var expires = "expires=" + d.toUTCString();
      if (value == "SimpleOff") {
          val = "Off";
      } else if (value == "SimpleOn") {
          val = "Full";
      } else {
          val = value;
      }

      document.cookie = "display_mode=" + val + "; " + expires;
      document.location.reload(true);
  }

  function set_stream_mode(value) {
      var d = new Date();
      d.setTime(d.getTime() + (365 * 24 * 60 * 60 * 1000));
      var expires = "expires=" + d.toUTCString();

      if (value == "DefaultStream") {
          document.getElementById("toggle_stream").value = "MJPEG-Stream";
      } else {
          document.getElementById("toggle_stream").value = "Default-Stream";
      }
      document.cookie = "stream_mode=" + value + "; " + expires;
      document.location.reload(true);
  }

  function schedule_rows() {
      var sun, day, fixed, mode;
      mode = parseInt(document.getElementById("DayMode").value);
      switch (mode) {
          case 0:
              sun = 'table-row';
              day = 'none';
              fixed = 'none';
              break;
          case 1:
              sun = 'none';
              day = 'table-row';
              fixed = 'none';
              break;
          case 2:
              sun = 'none';
              day = 'none';
              fixed = 'table-row';
              break;
          default:
              sun = 'table-row';
              day = 'table-row';
              fixed = 'table-row';
              break;
      }
      var rows;
      rows = document.getElementsByClassName('sun');
      for (i = 0; i < rows.length; i++)
          rows[i].style.display = sun;
      rows = document.getElementsByClassName('day');
      for (i = 0; i < rows.length; i++)
          rows[i].style.display = day;
      rows = document.getElementsByClassName('fixed');
      for (i = 0; i < rows.length; i++)
          rows[i].style.display = fixed;
  }

  function set_preset(value) {
      var values = value.split(" ");
      document.getElementById("video_width").value = values[0];
      document.getElementById("video_height").value = values[1];
      document.getElementById("video_fps").value = values[2];
      document.getElementById("MP4Box_fps").value = values[3];
      document.getElementById("image_width").value = values[4];
      document.getElementById("image_height").value = values[5];

      set_res();
  }

  function set_res() {
      send_cmd("px " + document.getElementById("video_width").value + " " + document.getElementById("video_height").value + " " + document.getElementById("video_fps").value + " " + document.getElementById("MP4Box_fps").value + " " + document.getElementById("image_width").value + " " + document.getElementById("image_height").value);
      update_preview_delay();
      updatePreview(true);
  }

  function set_ce() {
      send_cmd("ce " + document.getElementById("ce_en").value + " " + document.getElementById("ce_u").value + " " + document.getElementById("ce_v").value);

  }

  function set_preview() {
      send_cmd("pv " + document.getElementById("quality").value + " " + document.getElementById("width").value + " " + document.getElementById("divider").value);
      update_preview_delay();
  }

  function set_encoding() {
      send_cmd("qp " + document.getElementById("minimise_frag").value + " " + document.getElementById("initial_quant").value + " " + document.getElementById("encode_qp").value);
  }

  function set_roi() {
      send_cmd("ri " + document.getElementById("roi_x").value + " " + document.getElementById("roi_y").value + " " + document.getElementById("roi_w").value + " " + document.getElementById("roi_h").value);
  }

  function set_at() {
      send_cmd("at " + document.getElementById("at_en").value + " " + document.getElementById("at_y").value + " " + document.getElementById("at_u").value + " " + document.getElementById("at_v").value);
  }

  function set_ac() {
      send_cmd("ac " + document.getElementById("ac_en").value + " " + document.getElementById("ac_y").value + " " + document.getElementById("ac_u").value + " " + document.getElementById("ac_v").value);
  }

  function set_ag() {
      send_cmd("ag " + document.getElementById("ag_r").value + " " + document.getElementById("ag_b").value);
  }

  function send_macroUpdate(i, macro) {
      var macrovalue = document.getElementById(macro).value;
      if (!document.getElementById(macro + "_chk").checked) {
          macrovalue = "-" + macrovalue;
      }
      send_cmd("um " + i + " " + macrovalue);
  }

  function hashHandler() {
      switch (window.location.hash) {
          case '#full':
          case '#fullscreen':
              if (mjpeg_img !== null && document.getElementsByClassName("fullscreen").length == 0) {
                  toggle_fullscreen(mjpeg_img);
              }
              break;
          case '#normal':
          case '#normalscreen':
              if (mjpeg_img !== null && document.getElementsByClassName("fullscreen").length != 0) {
                  toggle_fullscreen(mjpeg_img);
              }
              break;
      }
  }

  //
  // System shutdow, reboot, settime
  //
  function sys_shutdown() {
      ajax_status.open("GET", "cmd_func.php?cmd=shutdown", true);
      ajax_status.send();
  }

  function sys_reboot() {
      ajax_status.open("GET", "cmd_func.php?cmd=reboot", true);
      ajax_status.send();
  }

  function sys_settime() {
      var strDate = document.getElementById("timestr").value;
      if (strDate.indexOf("-") < 0) {
          ajax_status.open("GET", "cmd_func.php?cmd=settime&timestr=" + document.getElementById("timestr").value, true);
          ajax_status.send();
      }
  }

  //
  // MJPEG
  //
  var mjpeg_img;
  var halted = 0;
  var previous_halted = 99;
  var mjpeg_mode = 0;
  var preview_delay = 0;
  var btn_class_p = "btn btn-primary"
  var btn_class_a = "btn btn-warning"

  function reload_img() {
      if (!halted) mjpeg_img.src = "cam_pic.php?time=" + new Date().getTime() + "&pDelay=" + preview_delay;
      else setTimeout("reload_img()", 500);
  }

  function error_img() {
      setTimeout("mjpeg_img.src = 'cam_pic.php?time=' + new Date().getTime();", 100);
  }

  function updatePreview(cycle) {
      if (mjpegmode) {
          if (cycle !== undefined && cycle == true) {
              mjpeg_img.src = "/updating.jpg";
              setTimeout("mjpeg_img.src = \"cam_pic_new.php?time=\" + new Date().getTime()  + \"&pDelay=\" + preview_delay;", 1000);
              return;
          }

          if (previous_halted != halted) {
              if (!halted) {
                  mjpeg_img.src = "cam_pic_new.php?time=" + new Date().getTime() + "&pDelay=" + preview_delay;
              } else {
                  mjpeg_img.src = "/unavailable.jpg";
              }
          }
          previous_halted = halted;
      }
  }

  //
  // Ajax Status
  //
  var ajax_status;

  if (window.XMLHttpRequest) {
      ajax_status = new XMLHttpRequest();
  } else {
      ajax_status = new ActiveXObject("Microsoft.XMLHTTP");
  }

  function createManualLabel(activityid, placeid, itemid, poseid, repetitionId) {
      var activity = document.getElementById(activityid);
      var place = document.getElementById(placeid);
      var item = document.getElementById(itemid);
      var pose = document.getElementById(poseid);
      var rep = document.getElementById(repetitionId);

      var selectedActivity = activity != null ? activity.options[activity.selectedIndex].text : "None";
      var selectedPlace = place != null ? place.options[place.selectedIndex].text : "None";
      var selectedItem = item != null ? item.options[item.selectedIndex].text : "None";
      var selectedPose = pose != null ? pose.options[pose.selectedIndex].text : "None";
      var selectedRep = rep != null ? rep.options[rep.selectedIndex].text : "None";
      return selectedActivity + "_" + selectedPlace + "_" + selectedItem + "_" + selectedPose + "_" + selectedRep;
  }

  function change_naming(activityid, placeid, itemid, poseid, taskelementid, repetitionId) {
      var label = createManualLabel(activityid, placeid, itemid, poseid, repetitionId);
      document.getElementById(taskelementid).innerHTML = label;
      changeName(label);
  }

  function change_freeform_name(value, id) {
      document.getElementById(id).innerHTML = value;
      changeName(value);
  }

  function getRandom(max) {
      return Math.floor(Math.random() * max);
  }

  function new_video_start(value) {
      document.getElementById(value).innerHTML = "New labeled video started...";
      startNewVideo(value);
  }

  function new_video_stop(value) {
      document.getElementById(value).innerHTML = "Labeled video stopped, new unlabeled video started...";
      changeName("_");
      startNewVideo(value);
  }

  function startNewVideo(value) {
      setTimeout(() => {
          send_cmd("ca 0");
      }, 1000);
      setTimeout(() => {
        changeName(currentLabel);
          send_cmd("ca 1");
          document.getElementById(value).innerHTML = "";
      }, 2000);
  }
  
  function getPoseAndItem(poseIdx) {
      var posesForActivitiesGym = ['standing', 'standing', 'sitting', 'sitting', 'lying', 'lying', 'lying', 'sitting', 'lying', 'standing', 'lying','standing', 'standing', 'standing', 'standing', 'sitting', 'sitting'];
      var itemsForActivitiesGym = ['Cardio', 'Cardio', 'Cardio', 'Cardio', 'None', 'None', 'None', 'None', 'None', 'None', 'dumbbells', 'dumbbells', 'dumbbells', 'dumbbells', 'machine', 'machine', 'machine'];
      return [posesForActivitiesGym[poseIdx], itemsForActivitiesGym[poseIdx]];
  } 

  function createLabel(activityid, placeid, itemid, poseid, repid, randomActivity) {
    
      var activity = document.getElementById(activityid);
      var place = document.getElementById(placeid);
      var item = document.getElementById(itemid);
      var pose = document.getElementById(poseid);
      var rep = document.getElementById(repid);
      var selectedPose = '';
      var selectedRep = '';
      var selectedItem = '';
      var selectedActivity = '';

      var selectedActivityOptions = activity.options.length;
      var selectedPlaceOptions = place.options.length;
      var selectedItemOptions = item.options.length;

      var activitynumber = randomActivity == 'gym' ? currentGymTaskIndex : getRandom(selectedActivityOptions-1);
      var selectedPlace = place.options[getRandom(selectedPlaceOptions)].text;
      
      if (currentShoppingTaskIndex >= selectedItemOptions-1) {
        selectedActivity = activity.options[8].text;
        var itemToReturnIdx = currentShoppingCart.pop();
        selectedItem = item.options[itemToReturnIdx].text;
      } else {
        selectedActivity = activity.options[activitynumber].text;
        if (itemid == '') {
          selectedItem = 'None';
        } else {
          var itemNumber = randomActivity == 'grocery' ? currentShoppingTaskIndex : getRandom(selectedItemOptions);
          selectedItem = item.options[itemNumber].text;
          if(activitynumber == 1 || activitynumber == 3 || activitynumber == 5 || activitynumber == 7) {
            currentShoppingCart.push(itemNumber);
          }
       }
      }
      
      if (poseid != '') {
          var selectedPoseOptions = pose.options.length;
          selectedPose = pose.options[getRandom(selectedPoseOptions - 3)].text;
      }
      
      if (randomActivity == 'gym') {
          var poseItem = getPoseAndItem(activitynumber);
          selectedPose = poseItem[0];
          selectedItem = poseItem[1];
          selectedRep = currentGymTaskIndex <= 5 ? 'None' : '10';
          selectedPlace = 'gym';
          currentGymTaskIndex = currentGymTaskIndex + 1;
      } else if (randomActivity == 'item' ) {
          selectedRep = 'None';
          selectedPlace = 'store';
      } else if (randomActivity == 'grocery') {
          selectedPlace = 'store';
          selectedRep = getRandom(5) + 1;
          currentShoppingTaskIndex = currentShoppingTaskIndex + 1;
      }
      return selectedActivity + "_" + selectedPlace + "_" + selectedItem + "_" + selectedPose + "_" + selectedRep;
  }

  function getCurrentDateString() {
      var date = new Date();
      var hour = date.getHours();
      var minute = date.getMinutes();
      var second = date.getSeconds();
      var millisecond = date.getMilliseconds();
      var day = date.getDate();
      var month = date.getMonth();
      var year = date.getFullYear();
      var dateString = year + "_" + month + "_" + day + "_" + hour + "_" + minute + "_" + second + "_" + millisecond;
      return dateString;
  }

  function append_label_start(taskelemid, feedback, kind) {
    document.getElementById(feedback).innerHTML = "TimeStamp for " + kind + " added";
      var dateString = getCurrentDateString();
      var label = kind + "," + dateString;
      appendAnnotation(label);
      setTimeout(() => {
          document.getElementById(feedback).innerHTML = "";
      }, 500);
  }
  
  function append_label_stop(feedback) {
    document.getElementById(feedback).innerHTML = "";
      if (currentAnnotation != ''){
          document.getElementById(feedback).innerHTML = "Label added";
          var dateString = getCurrentDateString();
          var annotation = currentAnnotation + ',' + dateString ;
          appendAnnotation(annotation);
          currentAnnotation = '';
      }
  }

  function nextTask(activityid, placeid, itemid, poseid, taskelementid, repid, randomActivity) {
      var label = createLabel(activityid, placeid, itemid, poseid, repid, randomActivity);
      changeName(label);
      document.getElementById(taskelementid).innerHTML = label;
  }

  ajax_status.onreadystatechange = function() {
      if (ajax_status.readyState == 4 && ajax_status.status == 200) {

          if (ajax_status.responseText == "ready") {
              document.getElementById("video_button").disabled = false;
              document.getElementById("video_button").value = "record video start";
              document.getElementById("video_button").onclick = function() {
                  changeName(currentLabel);
                  send_cmd("ca 1");
              }

              document.getElementById("image_button").disabled = false;
              document.getElementById("image_button").value = "record image";
              document.getElementById("image_button").onclick = function() {
                  send_cmd("im");
              };
              document.getElementById("timelapse_button").disabled = false;
              document.getElementById("timelapse_button").value = "timelapse start";
              document.getElementById("timelapse_button").onclick = function() {
                  send_cmd("tl 1");
              };
              document.getElementById("md_button").disabled = false;
              document.getElementById("md_button").value = "motion detection start";
              document.getElementById("md_button").onclick = function() {
                  send_cmd("md 1");
              };
              document.getElementById("halt_button").disabled = false;
              document.getElementById("halt_button").value = "stop camera";
              document.getElementById("halt_button").onclick = function() {
                  send_cmd("ru 0");
              };
              document.getElementById("preview_select").disabled = false;
              document.getElementById("video_button").className = btn_class_p;
              document.getElementById("timelapse_button").className = btn_class_p;
              document.getElementById("md_button").className = btn_class_p;
              document.getElementById("image_button").className = btn_class_p;
              halted = 0;
          } else if (ajax_status.responseText == "md_ready") {

              document.getElementById("video_button").disabled = true;
              document.getElementById("video_button").value = "record video start";
              document.getElementById("video_button").onclick = function() {};

              document.getElementById("image_button").disabled = false;
              document.getElementById("image_button").value = "record image";
              document.getElementById("image_button").onclick = function() {
                  send_cmd("im");
              };
              document.getElementById("timelapse_button").disabled = false;
              document.getElementById("timelapse_button").value = "timelapse start";
              document.getElementById("timelapse_button").onclick = function() {
                  send_cmd("tl 1");
              };
              document.getElementById("md_button").disabled = false;
              document.getElementById("md_button").value = "motion detection stop";
              document.getElementById("md_button").onclick = function() {
                  send_cmd("md 0");
              };
              document.getElementById("halt_button").disabled = true;
              document.getElementById("halt_button").value = "stop camera";
              document.getElementById("halt_button").onclick = function() {};
              document.getElementById("preview_select").disabled = false;
              document.getElementById("video_button").className = btn_class_p;
              document.getElementById("timelapse_button").className = btn_class_p;
              document.getElementById("md_button").className = btn_class_a;
              document.getElementById("image_button").className = btn_class_p;
              halted = 0;
          } else if (ajax_status.responseText == "timelapse") {
              document.getElementById("video_button").disabled = false;
              document.getElementById("video_button").value = "record video start";
              document.getElementById("video_button").onclick = function() {
                  send_cmd("ca 1");
              };
              document.getElementById("image_button").disabled = true;
              document.getElementById("image_button").value = "record image";
              document.getElementById("image_button").onclick = function() {};
              document.getElementById("timelapse_button").disabled = false;
              document.getElementById("timelapse_button").value = "timelapse stop";
              document.getElementById("timelapse_button").onclick = function() {
                  send_cmd("tl 0");
              };
              document.getElementById("md_button").disabled = true;
              document.getElementById("md_button").value = "motion detection start";
              document.getElementById("md_button").onclick = function() {};
              document.getElementById("halt_button").disabled = true;
              document.getElementById("halt_button").value = "stop camera";
              document.getElementById("halt_button").onclick = function() {};
              document.getElementById("preview_select").disabled = false;
              document.getElementById("video_button").className = btn_class_p;
              document.getElementById("timelapse_button").className = btn_class_a;
              document.getElementById("md_button").className = btn_class_p;
              document.getElementById("image_button").className = btn_class_p;
          } else if (ajax_status.responseText == "tl_md_ready") {
              document.getElementById("video_button").disabled = true;
              document.getElementById("video_button").value = "record video start";
              document.getElementById("video_button").onclick = function() {};
              document.getElementById("image_button").disabled = false;
              document.getElementById("image_button").value = "record image";
              document.getElementById("image_button").onclick = function() {
                  send_cmd("im");
              };
              document.getElementById("timelapse_button").disabled = false;
              document.getElementById("timelapse_button").value = "timelapse stop";
              document.getElementById("timelapse_button").onclick = function() {
                  send_cmd("tl 0");
              };
              document.getElementById("md_button").disabled = false;
              document.getElementById("md_button").value = "motion detection stop";
              document.getElementById("md_button").onclick = function() {
                  send_cmd("md 0");
              };
              document.getElementById("halt_button").disabled = true;
              document.getElementById("halt_button").value = "stop camera";
              document.getElementById("halt_button").onclick = function() {};
              document.getElementById("preview_select").disabled = false;
              document.getElementById("video_button").className = btn_class_p;
              document.getElementById("timelapse_button").className = btn_class_a;
              document.getElementById("md_button").className = btn_class_a;
              document.getElementById("image_button").className = btn_class_p;
              halted = 0;
          } else if (ajax_status.responseText == "video") {
              document.getElementById("video_button").disabled = false;
              document.getElementById("video_button").value = "record video stop";
              document.getElementById("video_button").onclick = function() {
                  send_cmd("ca 0");
              };

              document.getElementById("image_button").disabled = false;
              document.getElementById("image_button").value = "record image";
              document.getElementById("image_button").onclick = function() {
                  send_cmd("im");
              };
              document.getElementById("timelapse_button").disabled = false;
              document.getElementById("timelapse_button").value = "timelapse start";
              document.getElementById("timelapse_button").onclick = function() {
                  send_cmd("tl 1");
              };
              document.getElementById("md_button").disabled = true;
              document.getElementById("md_button").value = "motion detection start";
              document.getElementById("md_button").onclick = function() {};
              document.getElementById("halt_button").disabled = true;
              document.getElementById("halt_button").value = "stop camera";
              document.getElementById("halt_button").onclick = function() {};
              document.getElementById("preview_select").disabled = true;
              document.getElementById("video_button").className = btn_class_a;
              document.getElementById("timelapse_button").className = btn_class_p;
              document.getElementById("md_button").className = btn_class_p;
              document.getElementById("image_button").className = btn_class_p;
          } else if (ajax_status.responseText == "md_video") {
              document.getElementById("video_button").disabled = true;
              document.getElementById("video_button").value = "record video stop";
              document.getElementById("video_button").onclick = function() {};
              document.getElementById("image_button").disabled = false;
              document.getElementById("image_button").value = "record image";
              document.getElementById("image_button").onclick = function() {
                  send_cmd("im");
              };
              document.getElementById("timelapse_button").disabled = false;
              document.getElementById("timelapse_button").value = "timelapse start";
              document.getElementById("timelapse_button").onclick = function() {
                  send_cmd("tl 1");
              };
              document.getElementById("md_button").disabled = true;
              document.getElementById("md_button").value = "recording video...";
              document.getElementById("md_button").onclick = function() {};
              document.getElementById("halt_button").disabled = true;
              document.getElementById("halt_button").value = "stop camera";
              document.getElementById("halt_button").onclick = function() {};
              document.getElementById("preview_select").disabled = true;
              document.getElementById("video_button").className = btn_class_a;
              document.getElementById("timelapse_button").className = btn_class_p;
              document.getElementById("md_button").className = btn_class_a;
              document.getElementById("image_button").className = btn_class_p;
          } else if (ajax_status.responseText == "tl_video") {
              document.getElementById("video_button").disabled = false;
              document.getElementById("video_button").value = "record video stop";
              document.getElementById("video_button").onclick = function() {
                  send_cmd("ca 0");
              };
              document.getElementById("image_button").disabled = true;
              document.getElementById("image_button").value = "record image";
              document.getElementById("image_button").onclick = function() {
                  send_cmd("im");
              };
              document.getElementById("timelapse_button").disabled = false;
              document.getElementById("timelapse_button").value = "timelapse stop";
              document.getElementById("timelapse_button").onclick = function() {
                  send_cmd("tl 0");
              };
              document.getElementById("md_button").disabled = true;
              document.getElementById("md_button").value = "motion detection start";
              document.getElementById("md_button").onclick = function() {};
              document.getElementById("halt_button").disabled = true;
              document.getElementById("halt_button").value = "stop camera";
              document.getElementById("halt_button").onclick = function() {};
              document.getElementById("preview_select").disabled = false;
              document.getElementById("video_button").className = btn_class_a;
              document.getElementById("timelapse_button").className = btn_class_a;
              document.getElementById("md_button").className = btn_class_p;
              document.getElementById("image_button").className = btn_class_p;
          } else if (ajax_status.responseText == "tl_md_video") {
              document.getElementById("video_button").disabled = false;
              document.getElementById("video_button").value = "record video stop";
              document.getElementById("video_button").onclick = function() {
                  send_cmd("ca 0");
              };
              document.getElementById("image_button").disabled = true;
              document.getElementById("image_button").value = "record image";
              document.getElementById("image_button").onclick = function() {};
              document.getElementById("timelapse_button").disabled = false;
              document.getElementById("timelapse_button").value = "timelapse stop";
              document.getElementById("timelapse_button").onclick = function() {
                  send_cmd("tl 0");
              };
              document.getElementById("md_button").disabled = true;
              document.getElementById("md_button").value = "recording video...";
              document.getElementById("md_button").onclick = function() {};
              document.getElementById("halt_button").disabled = true;
              document.getElementById("halt_button").value = "stop camera";
              document.getElementById("halt_button").onclick = function() {};
              document.getElementById("preview_select").disabled = false;
              document.getElementById("video_button").className = btn_class_a;
              document.getElementById("timelapse_button").className = btn_class_a;
              document.getElementById("md_button").className = btn_class_a;
              document.getElementById("image_button").className = btn_class_p;
          } else if (ajax_status.responseText == "image") {
              document.getElementById("video_button").disabled = true;
              document.getElementById("video_button").value = "record video start";
              document.getElementById("video_button").onclick = function() {};
              document.getElementById("image_button").disabled = true;
              document.getElementById("image_button").value = "recording image";
              document.getElementById("image_button").onclick = function() {};
              document.getElementById("timelapse_button").disabled = true;
              document.getElementById("timelapse_button").value = "timelapse start";
              document.getElementById("timelapse_button").onclick = function() {};
              document.getElementById("md_button").disabled = true;
              document.getElementById("md_button").value = "motion detection start";
              document.getElementById("md_button").onclick = function() {};
              document.getElementById("halt_button").disabled = true;
              document.getElementById("halt_button").value = "stop camera";
              document.getElementById("halt_button").onclick = function() {};
              document.getElementById("preview_select").disabled = false;
              document.getElementById("image_button").className = btn_class_a;
          } else if (ajax_status.responseText == "halted") {
              document.getElementById("video_button").disabled = true;
              document.getElementById("video_button").value = "record video start";
              document.getElementById("video_button").onclick = function() {};
              document.getElementById("image_button").disabled = true;
              document.getElementById("image_button").value = "record image";
              document.getElementById("image_button").onclick = function() {};
              document.getElementById("timelapse_button").disabled = true;
              document.getElementById("timelapse_button").value = "timelapse start";
              document.getElementById("timelapse_button").onclick = function() {};
              document.getElementById("md_button").disabled = true;
              document.getElementById("md_button").value = "motion detection start";
              document.getElementById("md_button").onclick = function() {};
              document.getElementById("halt_button").disabled = false;
              document.getElementById("halt_button").value = "start camera";
              document.getElementById("halt_button").onclick = function() {
                  send_cmd("ru 1");
              };
              document.getElementById("preview_select").disabled = false;
              document.getElementById("video_button").className = btn_class_p;
              document.getElementById("timelapse_button").className = btn_class_p;
              document.getElementById("md_button").className = btn_class_p;
              document.getElementById("video_button").className = btn_class_p;
              document.getElementById("timelapse_button").className = btn_class_p;
              document.getElementById("md_button").className = btn_class_p;
              document.getElementById("image_button").className = btn_class_p;
              halted = 1;
          } else if (ajax_status.responseText.substr(0, 5) == "Error") alert("Error in RaspiMJPEG: " + ajax_status.responseText.substr(7) + "\nRestart RaspiMJPEG (./RPi_Cam_Web_Interface_Installer.sh start) or the whole RPi.");

          updatePreview();
          reload_ajax(ajax_status.responseText);

      }
  }

  function reload_ajax(last) {
      ajax_status.open("GET", "status_mjpeg.php?last=" + last, true);
      ajax_status.send();
  }

  //
  // Ajax Commands
  //
  var ajax_cmd;

  if (window.XMLHttpRequest) {
      ajax_cmd = new XMLHttpRequest();
  } else {
      ajax_cmd = new ActiveXObject("Microsoft.XMLHTTP");
  }

  function encodeCmd(s) {
      return s.replace(/&/g, "%26").replace(/#/g, "%23").replace(/\+/g, "%2B");
  }

  function send_cmd(cmd) {
      ajax_cmd.open("GET", "cmd_pipe.php?cmd=" + encodeCmd(cmd), true);
      ajax_cmd.send();
  }

  function changeName(label) {
    currentLabel = label;
    var date = getCurrentDateString();
      ajax_cmd.open("GET", "change_name.php?label=" + encodeCmd(label + "_" + date), true);
      ajax_cmd.send();
  }

  function appendAnnotation(annotation) {
      ajax_cmd.open("GET", "add_label.php?annotation=" + encodeCmd(annotation), true);
      ajax_cmd.send();
  }

  function update_preview_delay() {
      var video_fps = parseInt(document.getElementById("video_fps").value);
      var divider = parseInt(document.getElementById("divider").value);
      preview_delay = Math.floor(divider / Math.max(video_fps, 1) * 1000000);
  }

  //
  // Init
  //
  function init(mjpeg, video_fps, divider) {
      mjpeg_img = document.getElementById("mjpeg_dest");
      hashHandler();
      window.onhashchange = hashHandler;
      preview_delay = Math.floor(divider / Math.max(video_fps, 1) * 1000000);
      if (mjpeg) {
          mjpegmode = 1;
      } else {
          mjpegmode = 0;
          mjpeg_img.onload = reload_img;
          mjpeg_img.onerror = error_img;
          reload_img();
      }
      reload_ajax("");
  }
