Steps to Install:


1. Enable camera and Wifi in your Raspi settings

2. Download the setup_access_point.py file

3. Open the file and change the wpa_passphrase and ssid to avoid conflicts whith other Raspi networks. Make sure you choose a password longer than 8 characters.

4. Add your SSH Key to the project if necessary.

5. Then run $ python setup_access_point.py in the command line of the Raspi. This will clone this directory, install all necessary components and setup your RasPi as an access point.

6. Reboot the pi

7. You should then be able to connect to UbicomPi on any device and should see the video in your browser using the URL 192.168.4.1.


This project is based on the Web based interface for controlling the Raspberry Pi Camera, includeing motion detection, time lapse, and image and video recording from Silvan Melchior and contributors.
Current version 6.6.7
All information on this project can be found here: http://www.raspberrypi.org/forums/viewtopic.php?f=43&t=63276

The wiki page can be found here:

http://elinux.org/RPi-Cam-Web-Interface

This includes the installation instructions at the top and full technical details.
For latest change details see:

https://github.com/silvanmelchior/RPi_Cam_Web_Interface/commits/master
  
This version has updates for php7.3 / Buster. May need further changes for nginx
